//
//  BaseViewModel.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit
import RxSwift
import RxRelay

class BaseViewModel: NSObject {

    let disposeBag = DisposeBag()
    var showError = BehaviorRelay<String>.init(value: "")
    
    func onError(_ error: Error) {
        showError.accept(getErrorMsg(error: error))
    }
    
    func getErrorMsg(error: Error) -> String {
        let msg = (error as? LoadingError)?.errorMsg ?? L10n.Error.commonErrorMsg
        return msg
    }
}
