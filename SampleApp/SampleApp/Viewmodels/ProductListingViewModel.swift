//
//  ProductListingViewModel.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit
import RxRelay

protocol ProductListingPresentable: BasePagePresentable {
    var onDataChange: BehaviorRelay<[TicketModel]> {get}
}

class ProductListingViewModel: BaseViewModel, ProductListingPageListener {
    private let categoryId: Int
    weak private var presenter: ProductListingPresentable?
    
    required init(categoryId: Int) {
        self.categoryId = categoryId
    }
    
    convenience init(categoryId: Int, presenter: ProductListingPresentable?) {
        self.init(categoryId: categoryId)
        self.presenter = presenter
    }
    
    func becomeActive() {
        loadProductList()
    }
    
    private func loadProductList() {
        presenter?.updatePageState.accept(.loading)
        ProductServices.GetTicketListing
            .init(id: categoryId)
            .execute().subscribe {[weak self] res in
                self?.presenter?.onDataChange.accept(res ?? [])
                self?.presenter?.updatePageState.accept(.data)
        } onError: {[weak self] error in
            if let self = self {
                self.presenter?.updatePageState.accept(.error(self.getErrorMsg(error: error)))
            }
        } .disposed(by: disposeBag)
    }
}
