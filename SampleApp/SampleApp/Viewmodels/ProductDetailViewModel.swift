//
//  ProductDetailViewModel.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit
import RxRelay
import RxSwift

protocol ProductDetailPresentable: BasePagePresentable {
    var onDataChange: BehaviorRelay<TicketModel?> {get}
}

class ProductDetailViewModel: BaseViewModel, ProductDetailPageListener {

    weak private var presenter: ProductDetailPresentable?
    let productId: Int
    
    required init(productId: Int) {
        self.productId = productId
    }
    
    convenience init(productId: Int, presenter: ProductDetailPresentable?) {
        self.init(productId: productId)
        self.presenter = presenter
    }
    
    func onAddToCartClicked() {
        
    }
    
    func becomeActive() {
        loadTicketDetail()
    }
    
    private func loadTicketDetail() {
        presenter?.updatePageState.accept(.loading)
        ProductServices
            .GetTicketDetail
            .init(id: productId).execute()
            .subscribe {[weak self] ticket in
                self?.presenter?.onDataChange.accept(ticket)
                self?.presenter?.updatePageState.accept(.data)
            } onError: {[weak self] error in
                if let self = self {
                    self.presenter?.updatePageState.accept(.error(self.getErrorMsg(error: error)))
                }
            }.disposed(by: disposeBag)
    }
}
