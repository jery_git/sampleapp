//
//  CheckoutViewModel.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit
import RxSwift
import RxRelay

protocol CheckoutPagePresentable: BasePagePresentable {
    var onDataChange: BehaviorRelay<([CartModel], String?)> {get}
    var showPopupConfirmRemoveOrder: PublishSubject<CartModel> {get}
}

class CheckoutViewModel: BaseViewModel, CheckoutPageListener {
    
    weak var presenter: CheckoutPagePresentable?
    init(presenter: CheckoutPagePresentable?) {
        self.presenter = presenter
    }
    
    func onConfirmClicked() {
        
    }
    
    func onConfirmRemoveOrder(order: CartModel) {
        CartManager.shared().removeCartBy(id: order.ticket.id)
        let orders = CartManager.shared().getOrders()
        presenter?.onDataChange.accept((orders, getTotalPrice()))
        if orders.isEmpty {
            presenter?.updatePageState.accept(.empty(""))
        }
    }
    
    func onIncreaseAmountClicked(order: CartModel) {
        let orders = CartManager.shared().getOrders()
        CartManager.shared().increaseAmount(order: order)
        presenter?.onDataChange.accept((orders, getTotalPrice()))
    }
    
    func onDecreaseAmountClicked(order: CartModel) {
        if order.amount <= 1 {
            presenter?.showPopupConfirmRemoveOrder.onNext(order)
            return
        }
        CartManager.shared().decreaseAmount(order: order)
        presenter?.onDataChange.accept((CartManager.shared().getOrders(), getTotalPrice()))
    }
    
    func becomeActive() {
        loadData()
    }
    
    private func getTotalPrice() -> String? {
        let orders = CartManager.shared().getOrders()
        var price: Double = 0
        for order in orders {
            price += (Double(order.amount) * order.ticket.price)
        }
        return Utils.getPriceFormat(price)
    }
    
    private func loadData() {
        let order = CartManager.shared().getOrders()
        if order.isEmpty {
            presenter?.updatePageState.accept(.empty(""))
        } else {
            presenter?.onDataChange.accept((order, getTotalPrice()))
            presenter?.updatePageState.accept(.data)
        }
    }
}
