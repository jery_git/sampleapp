//
//  HomePageViewModel.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit
import RxRelay

protocol BasePagePresentable: AnyObject {
    var updatePageState: BehaviorRelay<PageState> {get}
}

protocol HomepagePresentable: BasePagePresentable {
    var updateTabData: BehaviorRelay<(Int, [CategoryModel])> {get}
}

class HomePageViewModel: BaseViewModel, HomePageListener {
    
    init(presenter: HomepagePresentable?) {
        self.presenter = presenter
    }
    
    var tabSelected = 0
    weak var presenter: HomepagePresentable?
    
    func becomeActive() {
        loadCategory()
    }
    
    func onCategorySelected(id: Int, index: Int) {
        guard let presenter = self.presenter else {return}
        let cate = presenter.updateTabData.value.1
        presenter.updateTabData.accept((index, cate))
    }
    
    private func onGetCategory(_ data: [CategoryModel]) {
        presenter?.updateTabData.accept((tabSelected, data))
        presenter?.updatePageState.accept(.data)
    }
    
    private func loadCategory() {
        presenter?.updatePageState.accept(.loading)
        ProductServices.GetCatory.init().execute().subscribe {[weak self] res in
            self?.onGetCategory(res ?? [])
        } onError: {[weak self] err in
            if let self = self {
                self.presenter?.updatePageState.accept(.error(self.getErrorMsg(error: err)))
            }
        }.disposed(by: disposeBag)
    }
}
