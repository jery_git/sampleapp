//
//  CartItemCollectionViewCell.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit

class CartItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtAmount: UILabel!
    @IBOutlet weak var txtTotal: UILabel!
    
    var onDecreaseClickedHander: ((CartModel) -> Void)?
    var onIncreaseClickedHander: ((CartModel) -> Void)?
    
    var order: CartModel? {
        didSet {
            txtName.text = order?.ticket.title
            txtAmount.text = "\(order?.amount ?? 0)"
            txtTotal.text = order?.getPriceFormatted()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func onDecreaseAmountClicked(_ sender: Any) {
        guard let order = self.order else {return}
        onDecreaseClickedHander?(order)
    }
    
    @IBAction func onIncreaseAmountClicked(_ sender: Any) {
        guard let order = self.order else {return}
        onIncreaseClickedHander?(order)
    }
}
