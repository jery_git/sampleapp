//
//  TicketCollectionViewCell.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit

class TicketCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var txtPrice: UILabel!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDes: UILabel!
    @IBOutlet weak var imgThumb: UIImageView!
    
    var ticket: TicketModel? {
        didSet {
            txtTitle.text = ticket?.title
            txtDes.text = ticket?.description
            txtPrice.text = ticket?.getPriceFormatted()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewInfo.layer.cornerRadius = 12
        viewInfo.layer.borderWidth = 1
        viewInfo.layer.borderColor = .init(red: 0.77, green: 0.77, blue: 0.77, alpha: 1.9)
        imgThumb.image = Asset.ticketThumb.image
    }
}
