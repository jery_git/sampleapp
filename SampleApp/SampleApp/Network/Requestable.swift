//
//  Requestable.swift
//  SampleApp
//
//  Created by Jery on 10/16/21.
//

import UIKit
import Alamofire
import RxSwift
import Foundation

struct APIConfigs {
    static let baseUrl = "https://app-stg.vouch.sg/"
}

enum LoadingError: Error {
    case notfound(String)
    case error(Int, String, HTTPURLResponse?)
}
extension LoadingError {
    var errorMsg: String? {
        switch self {
        case .notfound(let msg):
            return msg
        case .error(_, let msg, _):
            return msg
        }
    }
}

protocol Requestable {
    associatedtype Output
    
    var basePath: String { get }
    
    var endpoint: String { get }
    
    var httpMethod: HTTPMethod { get }
    
    var params: Parameters { get }
    
    var defaultHeaders: HTTPHeaders { get }
    
    var additionalHeaders: HTTPHeaders { get }
    
    var parameterEncoding: ParameterEncoding { get }
    
    var statusCode: Range<Int> { get }
    
    var timeout: Int {get}
    
    var contentType: [String] { get }
    
    func execute() -> Observable<Output>
    
    func decode(data: Any) -> Output
}

extension Requestable {
    
    var basePath: String {
        return APIConfigs.baseUrl
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var params: Parameters {
        return [:]
    }
    
    var defaultHeaders: HTTPHeaders {
        return ["Accept": "application/json"]
    }
    
    var additionalHeaders: HTTPHeaders {
        return [:]
    }

    var urlPath: String {
        return basePath + endpoint
    }
    
    var url: URL {
        return URL(string: urlPath)!
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.init()
    }
    
    var statusCode: Range<Int> {
        return 200..<300
    }
    
    var timeout: Int {
        return 10
    }
    
    var contentType: [String] {
        return ["application/json", "text/plain"]
    }
    
    @discardableResult
    func execute() -> Observable<Output> {
        return asObservable()
    }
    
    fileprivate func buildURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        urlRequest.timeoutInterval = .init(timeout)
        
        urlRequest = try parameterEncoding.encode(urlRequest, with: params)
        let headers = defaultHeaders
//
        for (key, val) in headers.dictionary {
            urlRequest.setValue(val, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    private func asObservable() -> Observable<Output> {
        return Observable.create { observer in
            guard let urlRequest = try? self.buildURLRequest() else {
                let errorResponse = LoadingError.notfound(L10n.Error.commonNotFound)
                observer.onError(errorResponse)
                return Disposables.create()
            }
            
            debugPrint(urlRequest)
            let request = AF.request(urlRequest)
                .validate(statusCode: statusCode)
                .responseJSON { response in
                switch response.result {
                case let .success(data):
                    observer.onNext(self.decode(data: data))
                    observer.onCompleted()
                case let .failure(error):
                    observer.onError(LoadingError.error(response.response?.statusCode ?? 0, error.errorDescription ?? "", response.response))
                }
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
}
