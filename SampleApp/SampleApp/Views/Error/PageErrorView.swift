//
//  PageErrorView.swift
//  SampleApp
//
//  Created by Jery on 10/16/21.
//

import UIKit

class PageErrorView: UIView {
    
    @IBOutlet var messageLabel: UILabel?
    @IBOutlet var iconImageView: UIImageView?
    
    var reloadHandler: (() -> Void)?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backgroungTapped))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func backgroungTapped() {
        self.reloadHandler?()
    }
    
    func showPageErrorMsg(_ msg: String?) {
        
    }
    
    static func newInstance() -> PageErrorView? {
        let view: PageErrorView = PageErrorView.fromNib()
        return view
    }
}
