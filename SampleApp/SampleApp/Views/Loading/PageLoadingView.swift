//
//  PageLoadingView.swift
//  SampleApp
//
//  Created by Jery on 10/16/21.
//

import UIKit

public class PageLoadingView: UIView {

    @IBOutlet public var activityIndicatorView: UIActivityIndicatorView?
    
    public func startLoading() {
        self.activityIndicatorView?.startAnimating()
    }
    
    public func stopLoading() {
        self.activityIndicatorView?.stopAnimating()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public static func newInstance() -> PageLoadingView? {
        let view: PageLoadingView = PageLoadingView.fromNib()
        return view
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
