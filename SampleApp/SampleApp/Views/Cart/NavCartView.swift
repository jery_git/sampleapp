//
//  NavCartView.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit

class NavCartView: UIButton {
    
    var txtBadge: UILabel!
    
    var onItemClicked: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
        self.updateCartStatus()
        
        AppNotificationCenter.cartChanged.addObserver(self, selector: #selector(onCartChanged), object: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        AppNotificationCenter.removeObserver(self)
    }
    
    private func initView() {
        setImage(Asset.navIcCart.image, for: .normal)
        addTarget(self, action: #selector(onCartViewClicked), for: .touchUpInside)

        let lblBadge = UILabel.init(frame: CGRect.init(x: 20, y: 3, width: 16, height: 16))
        lblBadge.backgroundColor = .red
        lblBadge.clipsToBounds = true
        lblBadge.layer.cornerRadius = 7
        lblBadge.textColor = UIColor.white
        lblBadge.textAlignment = .center
        lblBadge.font = .systemFont(ofSize: 10)
        lblBadge.textAlignment = .center

        txtBadge = lblBadge
        addSubview(lblBadge)
    }
    
    private func updateCartStatus() {
        let num = CartManager.shared().getOrderNums()
        txtBadge?.isHidden = num == 0
        if num > 100 {
            txtBadge.text = "99+"
        } else {
            txtBadge.text = "\(num)"
        }
    }
    
    @objc func onCartViewClicked() {
        onItemClicked?()
    }
    
    @objc func onCartChanged() {
        updateCartStatus()
    }
}
