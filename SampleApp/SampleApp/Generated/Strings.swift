// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum Cart {
    /// Are you want to remove this product?
    internal static let popupRemoveOrderMsg = L10n.tr("Localizations", "cart.popupRemoveOrderMsg")
    /// Cart
    internal static let title = L10n.tr("Localizations", "cart.title")
  }

  internal enum Common {
    /// Cancel
    internal static let cancel = L10n.tr("Localizations", "common.cancel")
    /// Nationality
    internal static let nationality = L10n.tr("Localizations", "common.nationality")
    /// Quantity
    internal static let quantity = L10n.tr("Localizations", "common.quantity")
    /// Remove
    internal static let remove = L10n.tr("Localizations", "common.remove")
  }

  internal enum Error {
    /// Something went wrong, Please try again!
    internal static let commonErrorMsg = L10n.tr("Localizations", "error.commonErrorMsg")
    /// Not found!
    internal static let commonNotFound = L10n.tr("Localizations", "error.commonNotFound")
  }

  internal enum Homepage {
    /// Browse
    internal static let headerBrowse = L10n.tr("Localizations", "homepage.headerBrowse")
  }

  internal enum Product {
    /// Add To Cart
    internal static let addToCart = L10n.tr("Localizations", "product.addToCart")
    /// Ticketing
    internal static let listingTitle = L10n.tr("Localizations", "product.listingTitle")
    /// Free
    internal static let priceFree = L10n.tr("Localizations", "product.priceFree")
  }

  internal enum ProductDetail {
    /// Ticket Detail
    internal static let title = L10n.tr("Localizations", "productDetail.title")
  }

  internal enum Region {
    /// Foreigner
    internal static let foreigner = L10n.tr("Localizations", "region.foreigner")
    /// Singaporean/PR
    internal static let singapore = L10n.tr("Localizations", "region.singapore")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
