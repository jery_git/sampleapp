//
//  AppstoryBoard.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit

enum AppStoryBoard: String {
    case main = "Main"
}

extension AppStoryBoard {
    
    func viewControllerWithId<T: UIViewController>(_ id: String) -> T? {
        let vc = value.instantiateViewController(withIdentifier: id)
        return vc as? T
    }
    
    private var value: UIStoryboard {
        return UIStoryboard.init(name: rawValue, bundle: Bundle.main)
    }
}
