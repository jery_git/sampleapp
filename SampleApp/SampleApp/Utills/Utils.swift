//
//  Utils.swift
//  SampleApp
//
//  Created by Jery on 10/20/21.
//

import UIKit

class Utils: NSObject {

    static func getPriceFormat(_ price: Double) -> String? {
        if price == 0 {
            return L10n.Product.priceFree
        }
        return "$ \(String.init(format: "%.2f", price))"
    }
}
