//
//  AppNotificationCenter.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit

enum AppNotificationCenter: String {
    case cartChanged = "cartChanged"
}

extension AppNotificationCenter {
    var value: NSNotification.Name {
        return .init(rawValue)
    }
    
    func post(object: Any?, userInfo: [AnyHashable: Any]?) {
        NotificationCenter.default.post(name: self.value, object: object, userInfo: userInfo)
    }
    
    func post(object: Any?) {
        NotificationCenter.default.post(name: self.value, object: object)
    }
    
    func addObserver(_ observer: Any, selector: Selector, object: Any?) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: self.value, object: object)
    }
    
    static func removeObserver(_ observer: Any) {
        NotificationCenter.default.removeObserver(observer)
    }
}
