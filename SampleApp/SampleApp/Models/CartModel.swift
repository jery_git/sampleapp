//
//  CartModel.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit

class CartModel: Codable {

    var ticket: TicketModel
    var amount = 0
    
    init(ticket: TicketModel, amount: Int = 0) {
        self.ticket = ticket
        self.amount = amount
    }
    
    func getPriceFormatted() -> String? {
        return Utils.getPriceFormat((ticket.price * Double(amount)))
    }
}
