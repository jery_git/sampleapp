//
//  CategoryModel.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit

class CategoryModel: Codable {
    var id: Int = 0
    var name: String
}
