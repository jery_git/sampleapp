//
//  ItemModel.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit

class TicketModel: Codable {

    var id: Int
    var price: Double
    var title: String?
    var description: String?
    
    private enum CodingKeys : String, CodingKey {
        case id, price, title, description
    }
    
    func getPriceFormatted() -> String? {
        return Utils.getPriceFormat(price)
    }
}
