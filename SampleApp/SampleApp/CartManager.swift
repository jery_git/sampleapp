//
//  CartManager.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit

class CartManager: NSObject {
    
    private static let instance = CartManager()
    
    private var orders: [CartModel] = []
    
    static func shared() -> CartManager {
        return instance
    }
    
    private override init() {
    }
    
    func getOrders() -> [CartModel] {
        return orders
    }
    
    func addToCart(ticket: TicketModel, amount: Int) {
        if let order = getOrderById(ticket.id) {
            order.amount += amount
        } else {
            let order = CartModel.init(ticket: ticket, amount: amount)
            orders.append(order)
        }
        print(orders)
        AppNotificationCenter.cartChanged.post(object: nil)
    }
    
    func increaseAmount(order: CartModel) {
        order.amount += 1
        AppNotificationCenter.cartChanged.post(object: nil)
    }
    
    func decreaseAmount(order: CartModel) {
        if order.amount <= 1 {
            return
        }
        
        order.amount -= 1
        AppNotificationCenter.cartChanged.post(object: nil)
    }
    
    func removeCartBy(id: Int) {
        for i in 0..<orders.count {
            if orders[i].ticket.id == id {
                orders.remove(at: i)
                break
            }
        }
        AppNotificationCenter.cartChanged.post(object: nil)
    }
    
    func getOrderNums() -> Int {
        return orders.count
    }
    
    func getOrderById(_ id: Int) -> CartModel? {
        return orders.first { c in
            return c.ticket.id == id
        }
    }
}
