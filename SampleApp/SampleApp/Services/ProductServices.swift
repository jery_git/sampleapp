//
//  ProductServices.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit

class ProductServices {
    
    struct Endpoints {
        static let category = "json-mock/ticketing/categories"
        static let product = "json-mock/ticketing/categories/%d"
        static let productDetail = "json-mock/ticketing/tickets/%d"
    }

    struct GetCatory: Requestable {
        typealias Output = [CategoryModel]?
        
        var endpoint: String {
            return Endpoints.category
        }
        
        func decode(data: Any) -> [CategoryModel]? {
            let jsonDecoder = JSONDecoder.init()
            if let json = data as? [[String: Any]],
               let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .fragmentsAllowed) {
                return try? jsonDecoder.decode([CategoryModel].self, from: jsonData)
            }
            return nil
        }
    }
    
    struct GetTicketListing: Requestable {
        var id: Int
        
        typealias Output = [TicketModel]?
        
        var endpoint: String {
            return String.init(format: Endpoints.product, id)
        }
        
        func decode(data: Any) -> [TicketModel]? {
            let jsonDecoder = JSONDecoder.init()
            if let json = data as? [[String: Any]],
               let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .fragmentsAllowed) {
                return try? jsonDecoder.decode([TicketModel].self, from: jsonData)
            }
            return nil
        }
    }
    
    struct GetTicketDetail: Requestable {
        var id: Int
        
        typealias Output = TicketModel?
        
        var endpoint: String {
            return String.init(format: Endpoints.productDetail, id)
        }
        
        func decode(data: Any) -> TicketModel? {
            let jsonDecoder = JSONDecoder.init()
            if let json = data as? [String: Any],
               let jsonData = try? JSONSerialization.data(withJSONObject: json, options: .fragmentsAllowed) {
                return try? jsonDecoder.decode(TicketModel.self, from: jsonData)
            }
            return nil
        }
    }
}
