//
//  ViewController.swift
//  SampleApp
//
//  Created by Jery on 10/16/21.
//

import UIKit
import RxSwift
import RxRelay

protocol BasePageListener: AnyObject {
    func becomeActive()
}

protocol HomePageListener: BasePageListener {
    func onCategorySelected(id: Int, index: Int)
}

class HomePageViewController: BaseViewController, HomepagePresentable {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtBrowse: UILabel!
    @IBOutlet weak var viewPageContainer: UIView!
    
    var updateTabData: BehaviorRelay<(Int, [CategoryModel])> = .init(value: (0, []))
    
    var listener: HomePageListener?
    private var pageViewController: UIPageViewController!
    private var pageChildViewControllers: [Int: ProductListingViewController] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = L10n.Product.listingTitle
        setupViews()
//        listener = HomePageViewModel(presenter: self)

        setupBindings()
        listener?.becomeActive()
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    private func setupViews() {
        setupLoadingView()
        setupErrorView()
        setupNav()
        txtBrowse.text = L10n.Homepage.headerBrowse
        
        collectionView.register(UINib.init(nibName: CategoryCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // page view
        pageViewController = .init()
        pageViewController.dataSource = self
        pageViewController.view.frame = viewPageContainer.bounds
        viewPageContainer.addSubview(pageViewController.view)
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageViewController?.view.frame = viewPageContainer.bounds
    }
    
    override func setupBindings() {
        super.setupBindings()
        updateTabData.skip(1).subscribe {[weak self] (index, category) in
            self?.onCategoryUpdate()
        }.disposed(by: disposeBag)
    }
}

extension HomePageViewController {
    
    private func onCategoryUpdate() {
        collectionView.reloadData()
        let cate = updateTabData.value.1
        
        if !cate.isEmpty {
            let page = getChildViewController(categoryId: cate[updateTabData.value.0].id, index: updateTabData.value.0)
            pageViewController?.setViewControllers([page], direction: .forward, animated: false, completion: nil)
        }
    }
}

extension HomePageViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return updateTabData.value.1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.identifier, for: indexPath) as! CategoryCollectionViewCell
        let isSelected = updateTabData.value.0 == indexPath.item
        cell.txtName.text = updateTabData.value.1[indexPath.item].name
        cell.indicator.isHidden = !isSelected
        cell.txtName.textColor = isSelected ? .black : UIColor.init(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let category = updateTabData.value.1[indexPath.item].name
        let width = category.widthWithConstrainedHeight(12, font: UIFont.systemFont(ofSize: 16)) + 24
        return .init(width: width, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        listener?.onCategorySelected(id: updateTabData.value.1[indexPath.item].id, index: indexPath.item)
    }
}

extension HomePageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let category = updateTabData.value.1
        guard let productViewController = viewController as? ProductListingViewController, !category.isEmpty, productViewController.pageIndex < category.count - 2 else {
            return nil
        }
        
        // not support swipe to change tab yet
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        // not support swipe to change tab yet
        return nil
    }
    
    private func getChildViewController(categoryId: Int, index: Int) -> ProductListingViewController {
        if let vc = pageChildViewControllers[categoryId] {
            return vc
        }
        let vc: ProductListingViewController = AppStoryBoard.main.viewControllerWithId(String.init(describing: ProductListingViewController.self)) as! ProductListingViewController
        vc.listener = ProductListingViewModel.init(categoryId: categoryId, presenter: vc)
        vc.pageIndex = index
        pageChildViewControllers[categoryId] = vc
        
        return vc
    }
}
