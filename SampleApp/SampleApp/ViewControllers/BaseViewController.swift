//
//  BaseViewController.swift
//  SampleApp
//
//  Created by Jery on 10/16/21.
//

import UIKit
import RxSwift
import RxRelay

enum PageState {
    case loading
    case error(String)
    case empty(String)
    case data
}

class BaseViewController: UIViewController {
    var pLoadingView: PageLoadingView?
    var pEmptyView: UIView?
    var pErrorView: PageErrorView?
    
    lazy var navCartView: NavCartView? = {
        var cartView: NavCartView = .init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
        return cartView
    }()
    var badgeView: UILabel?
    
    let disposeBag = DisposeBag()
    var updatePageState: BehaviorRelay<PageState> = .init(value: .loading)
    
    private var dataState = PageState.data

    override func viewDidLoad() {
        super.viewDidLoad()
        styleNavBar()
    }
    
    private func styleNavBar() {
        
        let mainColor = UIColor.init(red: 0.07, green: 0.2, blue: 0.38, alpha: 1)
        navigationController?.navigationBar.barTintColor = mainColor
        navigationController?.navigationBar.backgroundColor = mainColor

        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        navigationItem.backButtonTitle = ""
    }
    
    func setupNav() {
        guard let cartView = navCartView else {return}
        navigationItem.rightBarButtonItems = [UIBarButtonItem.init(customView: cartView)]
        cartView.onItemClicked = {[weak self] in
            self?.navigateToCheckout()
        }
    }
    
    func navigateToCheckout() {
        let checkout: CheckoutViewController = AppStoryBoard.main.viewControllerWithId(String.init(describing: CheckoutViewController.self))!
        let viewmodel = CheckoutViewModel.init(presenter: checkout)
        checkout.listener = viewmodel
        navigationController?.pushViewController(checkout, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupBindings() {
        updatePageState.subscribe {[weak self] state in
            self?.updateDataState(state)
        }.disposed(by: disposeBag)
    }
}

// Page state
extension BaseViewController {
    
    func getContentView() -> UIView {
        return view
    }
    
    func setupLoadingView() {
        if pLoadingView == nil {
            pLoadingView = .newInstance()
            let parentView = getContentView()
            view.addSubview(pLoadingView!)
            
            pLoadingView?.translatesAutoresizingMaskIntoConstraints = false
            pLoadingView?.topAnchor.constraint(equalTo: parentView.topAnchor).isActive = true
            pLoadingView?.leadingAnchor.constraint(equalTo: parentView.leadingAnchor).isActive = true
            pLoadingView?.trailingAnchor.constraint(equalTo: parentView.trailingAnchor).isActive = true
            pLoadingView?.bottomAnchor.constraint(equalTo: parentView.bottomAnchor).isActive = true
        }
        self.pLoadingView?.isHidden = true
    }
    
    func setupErrorView() {
        if pErrorView == nil {
            pErrorView = .newInstance()
            pErrorView?.reloadHandler = { [weak self] in
                self?.onRetry()
            }
            
            let parentView = getContentView()
            parentView.addSubview(pErrorView!)
            
            pErrorView?.translatesAutoresizingMaskIntoConstraints = false
            pErrorView?.topAnchor.constraint(equalTo: parentView.topAnchor).isActive = true
            pErrorView?.leadingAnchor.constraint(equalTo: parentView.leadingAnchor).isActive = true
            pErrorView?.trailingAnchor.constraint(equalTo: parentView.trailingAnchor).isActive = true
            pErrorView?.bottomAnchor.constraint(equalTo: parentView.bottomAnchor).isActive = true
        }
        self.pErrorView?.isHidden = true
    }
    
    func updateDataState(_ state: PageState){
        self.pLoadingView?.isHidden = true
        self.pErrorView?.isHidden = true
        self.pEmptyView?.isHidden = true
        
        self.pLoadingView?.stopLoading()
        
        switch state {
        case .loading:
            self.pLoadingView?.isHidden = false
            self.pLoadingView?.startLoading()
            break
        case .empty(_):
            self.pEmptyView?.isHidden = false
            break
        case .error(_):
            
            break
        default:
            break
        }
    }
    
    func onRetry() {
        
    }
}
