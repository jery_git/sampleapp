//
//  ProductListingViewController.swift
//  SampleApp
//
//  Created by Jery on 10/17/21.
//

import UIKit
import RxRelay

protocol ProductListingPageListener: BasePageListener {
    init(categoryId: Int)
}

class ProductListingViewController: BaseViewController, ProductListingPresentable {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var pageIndex: Int = 0
    var onDataChange = BehaviorRelay<[TicketModel]>(value: [])
    
    var listener: ProductListingPageListener?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupErrorView()
        setupLoadingView()
        setupViews()
        setupBindings()
        listener?.becomeActive()
    }
    
    private func setupViews() {
        collectionView.register(UINib.init(nibName: TicketCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: TicketCollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func setupBindings() {
        super.setupBindings()
        onDataChange.subscribe {[weak self] res in
            self?.collectionView.reloadData()
        }.disposed(by: disposeBag)
    }
}

extension ProductListingViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onDataChange.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TicketCollectionViewCell.identifier, for: indexPath) as! TicketCollectionViewCell
        cell.ticket = onDataChange.value[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.bounds.width, height: 108)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ticketId = onDataChange.value[indexPath.item].id
        navigateToProductDetail(ticketId)
    }
    
    private func navigateToProductDetail(_ id: Int) {
        let productDetail: ProductDetailViewController = AppStoryBoard.main.viewControllerWithId(String.init(describing: ProductDetailViewController.self))!
        productDetail.listener = ProductDetailViewModel.init(productId: id, presenter: productDetail)
        navigationController?.pushViewController(productDetail, animated: true)
    }
}
