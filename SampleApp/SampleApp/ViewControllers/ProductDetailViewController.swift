//
//  ProductDetailViewController.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit
import RxRelay

protocol ProductDetailPageListener: BasePageListener {
    init(productId: Int)
    func onAddToCartClicked()
}

class ProductDetailViewController: BaseViewController, ProductDetailPresentable {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtPrice: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var btnAddToCart: UIButton!
    
    var onDataChange = BehaviorRelay<TicketModel?>(value: nil)
    var listener: ProductDetailPageListener?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = L10n.ProductDetail.title
        setupViews()
        setupBindings()
        listener?.becomeActive()
        setupNav()
    }
    
    private func setupViews() {
        setupErrorView()
        setupLoadingView()
    }
    
    override func setupBindings() {
        super.setupBindings()
        onDataChange.subscribe {[weak self] ticket in
            self?.onGetTicket()
        }.disposed(by: disposeBag)
    }
    
    func onGetTicket() {
        imgCover.backgroundColor = UIColor.darkGray
        let ticket = onDataChange.value
        txtName.text = ticket?.title
        txtPrice.text = ticket?.getPriceFormatted()
        txtDescription.text = ticket?.description
        btnAddToCart.setTitle(L10n.Product.addToCart, for: .normal)
    }

    @IBAction func onAddToCardClicked(_ sender: Any) {
        guard let ticket = onDataChange.value else {return}
        
        let vc: AddToCardViewController = AppStoryBoard.main.viewControllerWithId(String.init(describing: AddToCardViewController.self))!
        vc.ticket = ticket
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
    }
}
