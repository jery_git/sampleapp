//
//  SuccedViewController.swift
//  SampleApp
//
//  Created by Jery on 10/20/21.
//

import UIKit

class SuccedViewController: BaseViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onBackToHomeClicked(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
}
