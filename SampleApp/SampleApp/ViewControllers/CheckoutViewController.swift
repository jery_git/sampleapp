//
//  CheckoutViewController.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit
import RxSwift
import RxRelay

protocol CheckoutPageListener: BasePageListener {
    func onConfirmClicked()
    func onIncreaseAmountClicked(order: CartModel)
    func onDecreaseAmountClicked(order: CartModel)
    func onConfirmRemoveOrder(order: CartModel)
}

class CheckoutViewController: BaseViewController, CheckoutPagePresentable {
    var onDataChange = BehaviorRelay<([CartModel], String?)>(value: ([], nil))
    var showPopupConfirmRemoveOrder = PublishSubject<CartModel>()
    
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var txtEmptyMsg: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var txtSubtotal: UILabel!
    @IBOutlet weak var txtPrice: UILabel!
    
    var listener: CheckoutPageListener?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = L10n.Cart.title
        setupViews()
        setupBindings()
        listener?.becomeActive()
    }
    
    private func setupViews() {
        collectionView.register(UINib.init(nibName: CartItemCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: CartItemCollectionViewCell.identifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.pEmptyView = txtEmptyMsg
    }
    
    override func setupBindings() {
        super.setupBindings()
        onDataChange.subscribe {[weak self] data in
            self?.onDataChanged()
        }.disposed(by: disposeBag)
        showPopupConfirmRemoveOrder.subscribe {[weak self] (order) in
            self?.showPopConfirmRemoveOrder(order: order)
        }.disposed(by: disposeBag)
    }
    
    private func onDataChanged() {
        // TODO refactor in case <-> single cell change
        self.collectionView.reloadData()
        self.txtPrice.text = onDataChange.value.1
    }
    
    private func showPopConfirmRemoveOrder(order: CartModel) {
        let refreshAlert = UIAlertController(title: nil, message: L10n.Cart.popupRemoveOrderMsg , preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: L10n.Common.remove, style: .default, handler: { (action: UIAlertAction!) in
            self.listener?.onConfirmRemoveOrder(order: order)
        }))
        refreshAlert.addAction(UIAlertAction(title: L10n.Common.cancel, style: .cancel, handler: { (action: UIAlertAction!) in
              
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func onConfirmClicked(_ sender: Any) {
        let succ: SuccedViewController = AppStoryBoard.main.viewControllerWithId(String.init(describing: SuccedViewController.self))!
        navigationController?.pushViewController(succ, animated: true)
    }
}

extension CheckoutViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onDataChange.value.0.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CartItemCollectionViewCell.identifier, for: indexPath) as! CartItemCollectionViewCell
        cell.order = onDataChange.value.0[indexPath.item]
        cell.onIncreaseClickedHander = {[weak self] order in
            self?.listener?.onIncreaseAmountClicked(order: order)
        }
        cell.onDecreaseClickedHander = {[weak self] order in
            self?.listener?.onDecreaseAmountClicked(order: order)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.bounds.width, height: 103)
    }
}
