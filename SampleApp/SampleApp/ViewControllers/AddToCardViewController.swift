//
//  AddToCardViewController.swift
//  SampleApp
//
//  Created by Jery on 10/19/21.
//

import UIKit
import RxCocoa
import RxSwift

enum RegionId: Int {
    case singapore = 0
    case foreigner
    
    var value: Int {
        return rawValue
    }
}

class AddToCardViewController: UIViewController {
    @IBOutlet weak var btnAddToCard: UIButton!
    @IBOutlet weak var txtAmount: UILabel!
    @IBOutlet weak var stpAmount: UIStepper!
    @IBOutlet weak var txtQuanity: UILabel!
    @IBOutlet weak var txtRegion: UILabel!
    @IBOutlet weak var btnSingapore: UIButton!
    @IBOutlet weak var btnForeigner: UIButton!
    @IBOutlet weak var txtname: UILabel!
    @IBOutlet weak var txtPrice: UILabel!
    
    let dispose = DisposeBag()
    var ticket: TicketModel!
    
    var regionIdSelected = RegionId.singapore
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black.withAlphaComponent(0.2)
        setupViews()
    }
    
    private func setupViews() {
        txtname.text = ticket.title
        txtPrice.text = ticket.getPriceFormatted()
        txtRegion.text = L10n.Common.nationality
        btnSingapore.setTitle(L10n.Region.singapore, for: .normal)
        btnForeigner.setTitle(L10n.Region.foreigner, for: .normal)
        txtQuanity.text = L10n.Common.quantity
        txtAmount.text = "0"
        btnAddToCard.setTitle(L10n.Product.addToCart, for: .normal)
        
        btnForeigner.tag = RegionId.foreigner.value
        btnSingapore.tag = RegionId.singapore.value
        
        styleRegionButton(btnForeigner)
        styleRegionButton(btnSingapore)
        
        updateRegionButtonState()
        
        stpAmount.minimumValue = 1
        
        stpAmount.rx.value.subscribe {[unowned self] value in
            let ammout = Int(value.element ?? 0)
            self.txtAmount.text = "\(ammout)"
        }.disposed(by: dispose)
    }
    
    private func styleRegionButton(_ button: UIButton) {
        button.layer.cornerRadius = button.bounds.height / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.init(red: 0.65, green: 0.65, blue: 0.65, alpha: 1).cgColor
    }
    
    @IBAction func onRegionClicked(_ sender: UIButton) {
        if let regionId = RegionId.init(rawValue: sender.tag) {
            regionIdSelected = regionId
            updateRegionButtonState()
        }
    }
    
    private func updateRegionButtonState() {
        let selectedColor = UIColor.init(red: 0.68, green: 0.68, blue: 0.68, alpha: 1)
        let unselectedColor = UIColor.white
        switch regionIdSelected {
        case .foreigner:
            btnForeigner.backgroundColor = selectedColor
            btnSingapore.backgroundColor = unselectedColor
            break
        case .singapore:
            btnSingapore.backgroundColor = selectedColor
            btnForeigner.backgroundColor = unselectedColor
            break
        }
    }
    
    @IBAction func onAddToCartClicked(_ sender: Any) {
        let ammount = Int(stpAmount.value)
        if ammount > 0 {
            CartManager.shared().addToCart(ticket: ticket, amount: ammount)
        }
    }
    
    @IBAction func onCloseClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
